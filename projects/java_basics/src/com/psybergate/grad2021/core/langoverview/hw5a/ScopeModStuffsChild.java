package com.psybergate.grad2021.core.langoverview.hw5a;

public class ScopeModStuffsChild extends ScopeModStuff{

  public ScopeModStuffsChild(final String firstName, final String lastName, final String studentNo, final int age){
    super(firstName, lastName, studentNo, age);
    method02();
    System.out.println("\n");
  }


}
