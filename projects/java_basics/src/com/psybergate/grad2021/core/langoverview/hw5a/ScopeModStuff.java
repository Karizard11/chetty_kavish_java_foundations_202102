package com.psybergate.grad2021.core.langoverview.hw5a;

public class ScopeModStuff {

  private String firstName;
  private String lastName;
  private String studentNo;
  private int var;
  private int age;

  public ScopeModStuff(final String firstName, final String lastName, final String studentNo, final int age){
    this.firstName = firstName;
    this.lastName = lastName;
    this.studentNo = studentNo;
    this.age = age;
    var = 6;
    method03();
    System.out.println("\n");
  }


  public static void method(){
    System.out.println("This is a public method");
  }

  static void method1(){
    System.out.println("This is a packaged method");
  }

  protected void method02(){
    System.out.println("This is a protected method");
  }

  private void method03(){
    System.out.println("This is a private method");
  }

}

