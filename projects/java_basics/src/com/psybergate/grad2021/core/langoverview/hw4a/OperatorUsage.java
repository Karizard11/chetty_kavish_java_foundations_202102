package com.psybergate.grad2021.core.langoverview.hw4a;

public class OperatorUsage {
  public static void main(String[] args) {
    int i = 1;
    Integer j = new Integer(2);
    Integer k = new Integer(2);
    System.out.println(4%3);
    System.out.println(i++);
    System.out.println(++i);

    if(i == 3) {
      System.out.println("That is true");
    }else{
      System.out.println("That was false");
    }

    if(j == k) {
      System.out.println("That is true");
    }else{
      System.out.println("That was false");
    }

    if(i == 3 & j == 3){
      System.out.println("This is true");
    }else{
      System.out.println("That was false");
    }

    if(i == 3 & j == 2){
      System.out.println("This is true");
    }else{
      System.out.println("That was false");
    }

    if(i == 1 && j ==2 ){
      System.out.println("This is super true");
    }else{
      if(j ==2){
        System.out.println("The second one it true");
      }
    }

    if(i == 3 && j ==2 ){
      System.out.println("This is super true");
    }else{
      if(j ==2){
        System.out.println("The second one it true");
      }
    }

    i += 1;
    System.out.println(i);

    int l = (i>2)? 2: 4;
    System.out.println(l);

    l = (i>5)? 2: 4;
    System.out.println(l);

    for(int z = 1; z<5; z++){
      switch(z){
        case 1:
          System.out.println("This happened once");
          break;
        case 2:
          System.out.println("This happened twice");
          break;
        case 3:
          System.out.println("This happened thrice");
          break;
        case 4:
          System.out.println("This happened fourth");
          break;
      }
    }

  }
}
