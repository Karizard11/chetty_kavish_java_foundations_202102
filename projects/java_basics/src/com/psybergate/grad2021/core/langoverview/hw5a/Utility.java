package com.psybergate.grad2021.core.langoverview.hw5a;


import static com.psybergate.grad2021.core.langoverview.hw5a.ScopeModStuff.*;

public class Utility {
  public static void main(String[] args) {
    method();
    method1();

    System.out.println("\n");

    ScopeModStuff scopeModStuff1 = new ScopeModStuff("john", "smith", "17027111", 42);
    ScopeModStuffsChild scopeModeStuff2 = new ScopeModStuffsChild("john", "James", "1234567", 20);
    //scopeModStuff1.method03();

  }
}
