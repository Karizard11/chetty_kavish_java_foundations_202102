package com.psybergate.grad2021.core.hw1a.withoutEnums;

public class Months {

  private int position;

  private Months(int position){
    this.position = position;
  }

  public static final Months JANUARY = new Months(1);
  public static final Months FEBRUARY = new Months(2);
  public static final Months MARCH = new Months(3);
  public static final Months APRIL = new Months(4);
  public static final Months MAY = new Months(5);
  public static final Months JUNE = new Months(6);
  public static final Months JULY = new Months(7);
  public static final Months AUGUST = new Months(8);
  public static final Months SEPTEMBER = new Months(9);
  public static final Months OCTOBER = new Months(10);
  public static final Months NOVEMBER = new Months(11);
  public static final Months DECEMBER = new Months(12);

  public int getPosition() {
    return position;
  }
}
