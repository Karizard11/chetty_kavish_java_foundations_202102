package com.psybergate.grad2021.core.hw1a.withoutEnums;

import static com.psybergate.grad2021.core.hw1a.withoutEnums.Days.*;
import static com.psybergate.grad2021.core.hw1a.withoutEnums.Months.*;

public class Client {

  public static void main(String[] args) {

    Date aprilFools2021 = new Date(2021, APRIL, 1, THURSDAY);
    Date birthday2021 = new Date(2021, MAY, 19, WEDNESDAY);
    Date keisBirthday2021 = new Date(2021, DECEMBER, 11, SATURDAY);


    printDate(aprilFools2021);
    printDate(birthday2021);
    printDate(keisBirthday2021);

  }

  public static void printDate(Date date){
    System.out.println(date.getYear() + "/ "+ date.getMonth() + "/ " + date.getDay() +
        " ( " + date.getDayOfTheWeek() + ")");
  }

}
