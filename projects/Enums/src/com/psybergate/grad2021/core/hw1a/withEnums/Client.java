package com.psybergate.grad2021.core.hw1a.withEnums;

import static com.psybergate.grad2021.core.hw1a.withEnums.Days.*;
import static com.psybergate.grad2021.core.hw1a.withEnums.Months.*;

public class Client {

  public static void main(String[] args) {
    System.out.println("Months.FEBRUARY = " + FEBRUARY.getName() + " (" + FEBRUARY.getShortName() + ") " + " ("
        + FEBRUARY.getPosition() + ") Number of" + " days: " + FEBRUARY.getNoOfDays());
    System.out.println("MONDAY.name() = " + MONDAY.name() + " (" + (MONDAY.ordinal() + 1) + ") "
        + MONDAY.getClass() + " " + MONDAY.getDeclaringClass().getSuperclass() + " " + MONDAY.hashCode());

    Dates date = new Dates(2021, JANUARY, 22, TUESDAY);
    System.out.println(date.getYear() + "/ " + date.getMonth().getName() + " (" + date.getMonth().getPosition() + ")" +
        "/ " + date.getDay() +
        " (" + date.getDayOfTheWeek().name() + ")");
  }

}
