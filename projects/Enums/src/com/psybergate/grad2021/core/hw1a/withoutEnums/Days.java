package com.psybergate.grad2021.core.hw1a.withoutEnums;

public class Days {

  private String name;

  private Days(String name) {
    this.name = name;
  }

  public static final Days MONDAY = new Days("Monday");
  public static final Days TUESDAY = new Days("Tuesday");
  public static final Days WEDNESDAY = new Days("Wednesday");
  public static final Days THURSDAY = new Days("Thursday");
  public static final Days FRIDAY = new Days("Friday");
  public static final Days SATURDAY = new Days("Saturday");
  public static final Days SUNDAY = new Days("Sunday");

  public String getName() {
    return name;
  }
}
