package com.psybergate.grad2021.core.hw1a.withEnums;

public enum Days {
  MONDAY(), TUESDAY(), WEDNESDAY(), THURSDAY(),
  FRIDAY(), SATURDAY(), SUNDAY();


  private Days(){
  }
}
