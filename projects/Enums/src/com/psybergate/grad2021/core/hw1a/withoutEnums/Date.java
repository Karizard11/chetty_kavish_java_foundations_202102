package com.psybergate.grad2021.core.hw1a.withoutEnums;

import java.security.InvalidParameterException;

public class Date {

  private int year;
  private Months month;
  private int day;
  private Days dayOfTheWeek;

  public Date(int year, Months month, int day, Days dayOfTheWeek) {
    this.year = year;
    if(month.equals(null)){
      throw new InvalidParameterException("This is not a valid month");
    }else {
      this.month = month;
    }
    this.day = day;
    if(dayOfTheWeek.equals(null)){
      throw new InvalidParameterException("This is not a valid day of the week");
    }else {
      this.dayOfTheWeek = dayOfTheWeek;
    }
  }

  public int getYear() {
    return year;
  }

  public int getMonth() {
    return month.getPosition();
  }

  public int getDay() {
    return day;
  }

  public String getDayOfTheWeek() {
    return dayOfTheWeek.getName();
  }
}

