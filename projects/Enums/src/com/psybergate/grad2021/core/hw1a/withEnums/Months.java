package com.psybergate.grad2021.core.hw1a.withEnums;

public enum Months {

  JANUARY(1,"January", "JAN", 31), FEBRUARY(2, "February", "FEB", 28),
  MARCH(3, "March", "MAR", 31), APRIL(4, "April", "APR", 30),
  MAY(5, "May", "MAY", 31), JUNE(6, "June", "JUN", 30),
  JULY(7, "July", "JUL", 31), AUGUST(8, "AUGUST","AUG",  31),
  SEPTEMBER(9, "September", "SEP", 30), OCTOBER(10, "October", "OCT", 31),
  NOVEMBER(11, "November", "Nov", 30), DECEMBER(12, "December", "DEC", 31);

  private int position;
  private String Name;
  private String shortName;
  private int noOfDays;

  private Months(int position, String name, String shortName, int noOfDays) {
    this.position = position;
    Name = name;
    this.shortName = shortName;
    this.noOfDays = noOfDays;
  }

  public int getPosition() {
    return position;
  }

  public String getName() {
    return Name;
  }

  public int getNoOfDays() {
    return noOfDays;
  }

  public String getShortName() {
    return shortName;
  }
}
