package com.psybergate.grad2021.core.hw1a.withEnums;

public class Dates {

  private final int year;
  private final Months month;
  private final int day;
  private final Days dayOfTheWeek;

  public Dates(final int year, Months month, int day, Days dayOfTheWeek){
    this.year = year;
    this.month = month;
    this.day = day;
    this.dayOfTheWeek = dayOfTheWeek;
  }

  public Days getDayOfTheWeek() {
    return dayOfTheWeek;
  }

  public int getDay() {
    return day;
  }

  public int getYear() {
    return year;
  }

  public Months getMonth() {
    return month;
  }

}
