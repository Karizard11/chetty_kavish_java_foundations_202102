package com.psybergate.grad2021.core.ce1a;

public class findString {

  public static void main(String[] args) {
    String s1 = "abc";
    String s2 = "abc";
    String s3 = "def";
    String s4 = new String("def");

    System.out.println("s1.charAt(0) = " + s1.charAt(0));
    System.out.println("s1.charAt(1) = " + s1.charAt(1));
    System.out.println("s1.charAt(2) = " + s1.charAt(2));
    // System.out.println("s1.charAt(3) = " + s1.charAt(3));
    System.out.println("\n");
    System.out.println("s1.toLowerCase() = " + s1.toLowerCase());
    System.out.println("s1.toUpperCase() = " + s1.toUpperCase());
    System.out.println("\n");
    System.out.println("s1.contains(a) = " + s1.contains("a"));
    System.out.println("s1.contains(d) = " + s1.contains("d"));
    System.out.println("\n");
    System.out.println("s1.concat(\"def\") = " + s1.concat("def"));
    System.out.println("\n");
    System.out.println("s1 = " + s1);
    System.out.println("\n");
    System.out.println("s1.length() = " + s1.length());
    System.out.println("toString() = " + s1.toString());
    System.out.println("s1.trim() = " + s1.trim());
    System.out.println("\n");
    System.out.println("s1.substring(0) = " + s1.substring(0));
    System.out.println("s1.substring(1) = " + s1.substring(1));
    System.out.println("s1.substring(2) = " + s1.substring(2));
    System.out.println("s1.substring(3) = " + s1.substring(3));
    System.out.println("\n");
    System.out.println("s1.equals(s2) = " + s1.equals(s2));
    System.out.println("s1.equals(s3) = " + s1.equals(s3));
    System.out.println("s3.equals(s4) = " + s3.equals(s4));
    System.out.println("s3 == s4 = " + (s3 == s4));
    System.out.println("\n");
    System.out.println("s1.contains(\"a\") = " + s1.contains("a"));
    System.out.println("s1.contains(\"d\") = " + s1.contains("d"));
    System.out.println("\n");
    System.out.println("s1.getClass() == s3.getClass() = " + (s1.getClass() == s3.getClass()));
    System.out.println("\n");
    System.out.println("s1.replace(\"a\", \"g\") = " + s1.replace("a", "g"));
    System.out.println("\n");
    System.out.println("s1.valueOf(1) = " + s1.valueOf(1));
    System.out.println("\n");
    System.out.println("s1.compareTo(s2) = " + s1.compareTo(s2));
    System.out.println("s1.compareTo(s3) = " + s1.compareTo(s3));
    System.out.println("s3.compareTo(s1) = " + s3.compareTo(s1));
    System.out.println("s3.compareTo(s4) = " + s3.compareTo(s4));
    System.out.println("\n");
    System.out.println("s1.contentEquals(\"abc\") = " + s1.contentEquals("abc"));
    System.out.println("s1.contentEquals(\"a\") = " + s1.contentEquals("a"));

    StringBuilder stringBuilder = new StringBuilder("abcdef");

    stringBuilder.append("a");
    System.out.println("stringBuilder = " + stringBuilder);
    stringBuilder.delete(1,2);
    System.out.println("stringBuilder = " + stringBuilder);
    stringBuilder.insert(4,"c");
    System.out.println("stringBuilder = " + stringBuilder);
    stringBuilder.reverse();
    System.out.println("stringBuilder. = " + stringBuilder);
    stringBuilder.toString();
    System.out.println("stringBuilder = " + stringBuilder);
    stringBuilder.append(2);
    System.out.println("stringBuilder = " + stringBuilder);
  }

}
