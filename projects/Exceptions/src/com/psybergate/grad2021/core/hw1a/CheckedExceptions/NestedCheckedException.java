package com.psybergate.grad2021.core.hw1a.CheckedExceptions;

import com.psybergate.grad2021.core.ce2a.MyCheckedException;

public class NestedCheckedException {

  public static void main(String[] args) {
    method01();
  }
  public static void method01(){
    method02();
  }

  public static void method02(){
    method03();
  }

  public static void method03(){
    method04();
  }

  public static void method04(){
    try {
      method05();
    } catch (MyCheckedException e) {
      e.printStackTrace();
    }
  }
  public static void method05() throws MyCheckedException{
      throw new MyCheckedException();
  }

}
