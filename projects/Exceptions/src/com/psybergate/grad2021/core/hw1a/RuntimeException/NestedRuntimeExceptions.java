package com.psybergate.grad2021.core.hw1a.RuntimeException;

public class NestedRuntimeExceptions {

  public static void main(String[] args) {
    method01();
  }

  public static void method01(){
    method02();
  }
  public static void method02(){
    method03();
  }
  public static void method03(){
    method04();
  }
  public static void method04(){
    method05();
  }
  public static void method05() throws MyRuntimeExceptions{
    throw new MyRuntimeExceptions();
  }
}
