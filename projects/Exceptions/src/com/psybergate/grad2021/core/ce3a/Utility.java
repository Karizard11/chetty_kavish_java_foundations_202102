package com.psybergate.grad2021.core.ce3a;


import java.io.IOException;

public class Utility {

  public static void main(String[] args) throws Exception{
    try {
      someMethod01();
    } catch (MyCheckedException02 e) {
    //  e.printStackTrace();
      throw new MyCheckedException01("This is the cause: ", e);
    }
  }

  public static void someMethod01() throws MyCheckedException02{
    try {
      someMethod02();
    } catch (IOException e) {
      throw new MyCheckedException02(e);
    }
  }

  public static void someMethod02() throws IOException {
    throw new IOException();
  }
}
