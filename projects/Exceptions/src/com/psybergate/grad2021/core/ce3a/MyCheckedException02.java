package com.psybergate.grad2021.core.ce3a;

import java.io.IOException;

public class MyCheckedException02 extends Throwable {
  public MyCheckedException02(Throwable cause) {
    super(cause);
    System.out.println("This is the reason: " + cause);
  }
}
