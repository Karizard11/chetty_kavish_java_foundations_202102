package com.psybergate.grad2021.core.ce2a;


import com.psybergate.grad2021.core.ce1a.MyCheckedException;

import java.sql.SQLOutput;

import static com.psybergate.grad2021.core.ce2a.ClassB.*;

public class ClassA {

  public static void main(String[] args) {
    SomeMethodA01();
    SomeMethodA02();
    SomeMethodA03();
    SomeMethodA04();
  }

  public static void SomeMethodA01() {
    try{
      SomeMethodB01();
    }catch(MyCheckedException myCheckedException){
      System.out.println("This is not handling it");
      myCheckedException.printStackTrace();
    }
  }

  public static void SomeMethodA02() {
    try {
      SomeMethodB02();
    }catch(MyRuntimeException myRuntimeException){
      System.out.println("This does not need to be handled");
    }


  }
  public static void SomeMethodA03() {
    try {
      SomeMethodB03();
    } catch (MyExtendedErrorException myExtendedErrorException) {
      System.out.println("This doesn't need to be handled");
    }
  }
  public static void SomeMethodA04() {
    try {
      SomeMethodB04();
    } catch(MyExtendedThrowableException myExtendedThrowableException){
      System.out.println("This is also not handling it");
      myExtendedThrowableException.printStackTrace();
    }
  }
}
