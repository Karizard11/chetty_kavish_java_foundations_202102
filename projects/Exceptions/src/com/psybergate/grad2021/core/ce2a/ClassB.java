package com.psybergate.grad2021.core.ce2a;

import com.psybergate.grad2021.core.ce1a.MyCheckedException;

public class ClassB {

  public static void SomeMethodB01() throws MyCheckedException {
    throw new MyCheckedException();
  }
  public static void SomeMethodB02() throws MyRuntimeException{
    throw new MyRuntimeException();
  }
  public static void SomeMethodB03() throws MyExtendedErrorException{
    throw new MyExtendedErrorException();
  }
  public static void SomeMethodB04() throws MyExtendedThrowableException{
    throw new MyExtendedThrowableException();
  }

}
