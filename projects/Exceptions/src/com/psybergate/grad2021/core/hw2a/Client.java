package com.psybergate.grad2021.core.hw2a;


import static com.psybergate.grad2021.core.hw2a.ClassA.*;

public class Client {

  public static void main(String[] args) {
    try {
      methodA();
    } catch (ApplicationException e) {
      e.printStackTrace();
    }
  }

}
