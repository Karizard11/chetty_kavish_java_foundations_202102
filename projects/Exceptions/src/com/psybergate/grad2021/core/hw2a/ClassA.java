package com.psybergate.grad2021.core.hw2a;

import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException{
    try {
      methodB();
    } catch (Exception e) {
      System.out.println("I am not sure of to handle this");
    }
  }

  public static void methodB() throws Exception{
    try{
      methodC();
    }catch(SQLException sqlException) {
      sqlException.printStackTrace();
    }
  }

  public static void methodC() throws SQLException{
    throw new SQLException();
  }


}
