package com.psybergate.grad2021.core.hw4a;

import java.security.InvalidParameterException;

public class Account {

  private final String accountNumber;
  private final String accountHolderName;
  private final String accountHolderSurname;
  private final String accountType;
  protected double accountBalance;

  public Account(final String accountNumber,final String accountHolderName, final String accountHolderSurname,
                 final String accountType, double accountBalance) {
    this.accountNumber = accountNumber;
    this.accountHolderName = accountHolderName;
    this.accountHolderSurname = accountHolderSurname;
    this.accountType = accountType;
    this.accountBalance = accountBalance;
  }

  public void deposit(double amount){
    if(amount < 0) {
      throw new InvalidParameterException("Cannot add negative amount to balance");
    }else{
      this.accountBalance += amount;
    }

  }

  public boolean equals(Object object){
    if(this == object){
      return true;
    }
    if(object == null){
      return false;
    }
    if(!(this.getClass() == object.getClass())){
      return false;
    }

    Account account = (Account) object;

    return this.accountNumber.equals(account.accountNumber);
  }



  public String getAccountNumber() {
    return accountNumber;
  }

  public String getAccountHolderName() {
    return accountHolderName;
  }

  public String getAccountHolderSurname() {
    return accountHolderSurname;
  }

  public String getAccountType() {
    return accountType;
  }

}
