package com.psybergate.grad2021.core.ce1a;

public class Utility {

  public static void main(String[] args){
    someMethod01();
    System.out.println("This is an example of Exception squashing");
  }

  public static void someMethod01(){
    try{
     someMethod02();
    } catch (MyCheckedException myCheckedException){
      myCheckedException.printStackTrace();

    }
  }

  public static void someMethod02() throws MyCheckedException{
    throw new MyCheckedException();
  }


}
