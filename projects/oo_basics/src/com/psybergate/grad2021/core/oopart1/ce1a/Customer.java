package com.psybergate.grad2021.core.oopart1.ce1a;

import java.util.*;

public class Customer {

  private int customerNum;
  private String name;
  private String address;
  private List<CurrentAccount> caList = new ArrayList();

  public Customer(int customerNum, String name, String address){
    this.customerNum = customerNum;
    this.name = name;
    this.address = address;
  }
  
  public void addCurrentAccount(int accountNum, int balance) {
    System.out.println("Hello");
    CurrentAccount ca = new CurrentAccount(accountNum,balance);
    caList.add(ca);
  }

  public void printCA(){
    int total = 0;
    int cnt = 0;

    for(CurrentAccount ca: caList){
      System.out.println("Acc no.: " + ca.getAccountNum() + ", Acc Balance: " + ca.getBalance());
      total += ca.getBalance();
      cnt++;
    }
    System.out.println("The total balance of all " + cnt + " accounts is R" + total);
  }

}
