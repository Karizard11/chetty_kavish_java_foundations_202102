package com.psybergate.grad2021.core.oopart1.ce2a;


public class CurrentAccount extends Account{

  private int interestRate;
  private int overdraft;
  private final int MAX_OVERDRAFT = 100000;
  private boolean output = false;

  public CurrentAccount(String accountNum, String name, String surname,
                        String accountType, int balance, int overdraft, int interestRate){
    super(accountNum, name, surname,accountType, balance);
    this.interestRate = interestRate;
    this.overdraft = overdraft;
  }

  public boolean needsToBeReviewed(){
    if(output){
      System.out.println("This account needs to be reviewed");
    }else{
      System.out.println("This account is fine");
    }
    return output;
  }

  public void isOverdrawn(){
    if(getBalance() <0){
      System.out.println("This account is overdrawn");
      if(getBalance() < -1*(overdraft*0.2) | getBalance() <= -50000){
        output = true;
      }else{
        output = false;
      }
    }else {
      System.out.println("This account is not overdrawn");
    }
  }

}
