package com.psybergate.grad2021.core.oopart1.ce1a;

public class Utility {

  public static void main(String[] args) {
    Customer john = new Customer(1, "John", "1 Oxford Rd, Johannesburg, Gauteng, 2011");

    john.addCurrentAccount(1,1000);
    john.addCurrentAccount(2,200);
    john.addCurrentAccount(3,352);
    john.addCurrentAccount(4,6001);

    john.printCA();
  }
}
