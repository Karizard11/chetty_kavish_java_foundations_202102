package com.psybergate.grad2021.core.oopart1.ce3b;

public class Employee {

  private int age;
  private int empNum;
  private int salary;
  private int noOfLeaveDays;

  private String idNo;
  private String name;
  private String surname;



  public Employee( int age, String name, String surname, int empNum, String idNo,
                   int salary, int noOfLeaveDays){
    this.age = age;
    this.name = name;
    this.surname = surname;
    this.empNum = empNum;
    this.idNo = idNo;
    this.salary = salary;
    this.noOfLeaveDays = noOfLeaveDays;
  }

  public void doSomeThing(int i){
    System.out.println(" This is i: " + i);

  }

}
