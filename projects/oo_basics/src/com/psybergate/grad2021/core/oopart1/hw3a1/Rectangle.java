package com.psybergate.grad2021.core.oopart1.hw3a1;

public class Rectangle {

  private double length;
  private double width;

  private final int MAXIMUM_LENGTH = 200;
  private final int MAXIMUM_WIDTH = 100;
  private final int MAXIMUM_AREA = 15000;


  public Rectangle(double length, double width) {
    if (isNotValidRectangle(length, width)) {
      System.out.println("These are not valid rectangle dimensions");
    } else {
      this.length = length;
      this.width = width;
    }
  }

  public static double calculateArea(Rectangle rectangle) {
    final int MAXIMUM_AREA = 15000;
    double area = rectangle.length * rectangle.width;
    if (!(rectangle.isNotValidRectangle(rectangle.length, rectangle.width)) && area <= MAXIMUM_AREA) {
      return area;
    }else {
      System.out.println("Area is too large");
      return 0;
    }
  }

  public static double calculatePerimeter(Rectangle rectangle) {
    if(!(rectangle.isNotValidRectangle(rectangle.length, rectangle.width)))
      return 2 * (rectangle.length + rectangle.width);
    else{
      return 0;
    }
  }

  public static void printRectangle(Rectangle rectangle) {
    System.out.println("Length of rectangle: " + rectangle.length + "\n"
        + "Width of rectangle: " + rectangle.width + "\n"
        + "Area of rectangle: " + calculateArea(rectangle) + "\n"
        + "Perimeter of rectangle: " + calculatePerimeter(rectangle));
  }

  public boolean isNotValidRectangle(double length, double width) {
    if (length < width) {
      return true;
    } else {
      if (length > MAXIMUM_LENGTH) {
        System.out.println("Length is too large");
        return true;
      }
      if (width > MAXIMUM_WIDTH) {
        System.out.println("Width is too large");
        return true;
      }
      return false;

    }
  }
}
