package com.psybergate.grad2021.core.oopart1.ce3b;

public class Manager extends Employee{

  private int noOfEmployees;
  private String jobTitle;
  private float noOfLeaveDaysLeft;


  public Manager( int age, String name, String surname, int empNum, String idNo,
                  int salary, int noOfLeaveDays,
                  int noOfEmployees, String jobTitle){
    super(age, name, surname,empNum, idNo, salary, noOfLeaveDays);
    this.noOfEmployees = noOfEmployees;
    this. jobTitle = jobTitle;
  }

  public Manager( int age, String name, String surname, int empNum, String idNo, int salary, int noOfLeaveDays,
           int noOfEmployees, String jobTitle, int noOfLeaveDaysLeft){
    this(age, name, surname, empNum, idNo, salary, noOfLeaveDays,noOfEmployees, jobTitle);
    this.noOfLeaveDaysLeft = noOfLeaveDaysLeft;
  }

  public static void thisTrial(Manager out){
    System.out.println(out);
  }

  public void doSomeThing(int i){
    System.out.println("This is the Managers.i: " + i);
  }

}
