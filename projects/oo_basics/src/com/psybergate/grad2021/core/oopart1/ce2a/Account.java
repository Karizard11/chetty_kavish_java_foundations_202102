package com.psybergate.grad2021.core.oopart1.ce2a;

public class Account {

  private String accountNum;
  private String name;
  private String surname;
  private String accountType;
  private int balance;

  public Account(String accountNum, String name, String surname, String accountType, int balance){
    this.accountNum = accountNum;
    this.name = name;
    this.surname = surname;
    this.accountType = accountType;
    this.balance = balance;
  }

  public boolean needsToBeReviewed(){
    return true;
  }
  public String getAccountType(){
    return accountType;
  }

  public int getBalance(){
    return balance;
  }
}
