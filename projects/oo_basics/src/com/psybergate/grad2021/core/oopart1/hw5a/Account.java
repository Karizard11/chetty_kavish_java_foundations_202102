package com.psybergate.grad2021.core.oopart1.hw5a;

public abstract class Account {

  public abstract String getAccountNumber();

  public abstract int getBalance();
}
