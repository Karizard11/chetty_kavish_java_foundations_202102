package com.psybergate.grad2021.core.oopart1.hw5a;

public class CustomerUtils {

  public static void main(String[] args) {

    Customer customer = new Customer( "001", "John",
        21, "1 Oxford Rd", "Person" );

    createAccounts(customer);
    printBalances(customer);
  }

  private static void createAccounts(Customer customer) {
    Account account1 = new SavingsAccount("001", 5000, 1000);
    Account account2 = new CurrentAccount("001", 10000, 100);

    customer.addAccount(account1);
    customer.addAccount(account2);
  }

  public static void printBalances(Customer customer){
    for (Account account : customer.getAccounts()) {
      System.out.println("Customer Number: "+customer.getCustomerNumber() + " / Account Number: " +
          account.getAccountNumber() + " / Account Balance: " + account.getBalance());
    }
  }

}
