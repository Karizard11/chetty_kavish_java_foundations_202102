package com.psybergate.grad2021.core.oopart1.ce2a;

public class Utility {
  public static void main(String[] args) {
    CurrentAccount currentAccount = new CurrentAccount("001", "John",
        "Connors", "Savings", -21, 100, 10 );

    currentAccount.isOverdrawn();
    currentAccount.needsToBeReviewed();
  }
}
