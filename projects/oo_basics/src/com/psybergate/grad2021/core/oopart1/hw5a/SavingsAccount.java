package com.psybergate.grad2021.core.oopart1.hw5a;

public class SavingsAccount extends Account{

  private int minBalance;
  private String accountNumber;
  private int balance;

  public SavingsAccount(String accountNumber, int balance,  int minBalance) {
    this.minBalance = minBalance;
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public int getBalance() {
    return balance;
  }
}
