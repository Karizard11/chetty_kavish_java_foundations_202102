package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends Account{

  private int overdraft;
  private String accountNumber;
  private int balance;

  public CurrentAccount(String accountNumber, int balance, int overdraft){
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.overdraft = overdraft;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public int getBalance() {
    return balance;
  }
}
