package com.psybergate.grad2021.core.oopart2.hw5a3;

public class InternationalOrder extends Order {

  private double importDuties;
  private String orderType;

  public  InternationalOrder( int orderNumber, double importDuties, Customer customer, int discountType){
    super(orderNumber, discountType, customer);
    this.importDuties = importDuties;
    this.orderType = "International";
  }

  public double getOrderPrice(){
    if(getDiscountType() == 1){
      return applyImportDuties(applyDiscount(calcDiscountRateBasedOnYears()));
    }
    if(getDiscountType() == 2){
      return applyImportDuties(applyDiscount(calcDiscountRateBasedOnTotalPrice()));
    }
    return 0;
  }

  public double applyImportDuties(double discountedPrice) {
    return  discountedPrice + importDuties;
  }

  public boolean isInternational(){
    return true;
  }

  public String getOrderType(){
    return orderType;
  }
}
