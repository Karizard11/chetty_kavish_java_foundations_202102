package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public final class Money {

  private BigDecimal number;

  public Money(int number){
    this.number = BigDecimal.valueOf(number);
  }

  public BigDecimal add(int num){
    return this.number.add(BigDecimal.valueOf(num));
  }

  public BigDecimal getNumber() {
    return number;
  }
}

