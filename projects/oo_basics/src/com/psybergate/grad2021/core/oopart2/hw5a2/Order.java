package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.util.List;

public abstract class Order {

  private int orderNumber;

  public Order(int orderNumber){
    this.orderNumber = orderNumber;
  }


  public int getOrderNumber(){
    return orderNumber;
  }

  public abstract double getOrderPrice();
  public abstract String getOrderType();
  public abstract void addOrderItem(OrderItem orderitem);
  public abstract List<OrderItem> getOrderItems();
}
