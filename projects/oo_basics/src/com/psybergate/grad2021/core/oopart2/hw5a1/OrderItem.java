package com.psybergate.grad2021.core.oopart2.hw5a1;

public class OrderItem {

  private int orderItemQuantity;
  private Product product;
  private int orderItemNumber;

  public OrderItem( int orderItemNumber,int orderItemQuantity, Product product) {
    this.orderItemNumber = orderItemNumber;
    this.orderItemQuantity = orderItemQuantity;
    this.product = product;
  }

  public int getOrderItemNumber(){
    return orderItemNumber;
  }

  public int getOrderItemQuantity() {
    return orderItemQuantity;
  }

  public double getOrderItemPrice(){
    return product.getProductPrice();
  }

  public String getOrderItemName(){
    return product.getProductName();
  }

}