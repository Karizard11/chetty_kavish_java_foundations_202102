package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  private String orderType;
  private List<OrderItem> orderItems = new ArrayList();
  private Customer customer;

  public LocalOrder(int orderNumber, Customer customer){
    super(orderNumber);
    this.orderType = "Local";
    this.customer = customer;
  }

  public double calcDiscountRate(){
    if(this.customer.getCustomerAge() <= 0){
      return 0;
    }
    if(this.customer.getCustomerAge() >2 && this.customer.getCustomerAge() <= 5){
      return 0.075;
    }
    if(this.customer.getCustomerAge() > 5){
      return 0.125;
    }
    return 0;
  }

  public String getOrderType(){
    return orderType;
  }

  public double getOrderPrice(){
    double orderPrice = 0;

    for(OrderItem orderItem: orderItems){
      orderPrice += orderItem.getOrderItemPrice()*(1-calcDiscountRate())*orderItem.getOrderItemQuantity();
    }
    return orderPrice;
  }

  public void addOrderItem(OrderItem orderitem){
    orderItems.add(orderitem);
  }

  public List<OrderItem> getOrderItems(){
    return orderItems;
  }
}
