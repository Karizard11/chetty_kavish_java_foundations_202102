package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  private String orderType;

  public LocalOrder(int orderNumber, Customer customer, int discountType){
    super(orderNumber, discountType, customer);
    this.orderType = "Local";
  }

  public boolean isLocal(){
    return true;
  }

  public String getOrderType(){
    return orderType;
  }

}
