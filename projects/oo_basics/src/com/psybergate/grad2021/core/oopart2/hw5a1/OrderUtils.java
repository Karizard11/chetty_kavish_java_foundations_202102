package com.psybergate.grad2021.core.oopart2.hw5a1;

import com.sun.javafx.css.converters.PaintConverter;
import com.sun.org.apache.xpath.internal.operations.Or;

public class OrderUtils {

  public static void main(String[] args) {

    Customer customer1 = new Customer("Goku", "Son", 1,
        "Local", 3);

    Product product1 = new Product("Biscuits", 10.99);
    Product product2 = new Product("Rice", 25.99);
    Product product3 = new Product("Butter", 50.99);
    Product product4 = new Product("Meat", 79.99);

    OrderItem orderItem1 = new OrderItem(1,10, product1);
    OrderItem orderItem2 = new OrderItem(2,10, product2);
    OrderItem orderItem3 = new OrderItem(3,10, product3);
    OrderItem orderItem4 = new OrderItem(4,10, product4);

    Order order1 = new LocalOrder(1, 0.15);
    order1.addOrderItem(orderItem1);
    order1.addOrderItem(orderItem2);
    order1.addOrderItem(orderItem3);
    order1.addOrderItem(orderItem4);

    Product product5 = new Product("Noodles", 9.99);
    Product product6 = new Product("Fish", 70.99);
    Product product7 = new Product("Eggs", 30.99);

    OrderItem orderItem5 = new OrderItem(1,10, product5);
    OrderItem orderItem6 = new OrderItem(2,10, product6);
    OrderItem orderItem7 = new OrderItem(3,10, product7);


    Order order2 = new LocalOrder(2, 0.1);
    order2.addOrderItem(orderItem5);
    order2.addOrderItem(orderItem6);
    order2.addOrderItem(orderItem7);

    Product product8 = new Product("Vegetables", 19.99);
    Product product9 = new Product("Fast Food", 70.99);

    OrderItem orderItem8 = new OrderItem(1,10, product8);
    OrderItem orderItem9 = new OrderItem(2,5, product9);


    Order order3 = new LocalOrder(3, 0.2);
    order3.addOrderItem(orderItem8);
    order3.addOrderItem(orderItem9);

    Product product10 = new Product("Fruit", 19.99);

    OrderItem orderItem10 = new OrderItem(1, 3, product10);

    Order order4 = new LocalOrder(4, 0.25);
    order4.addOrderItem(orderItem10);

    customer1.addOrder(order1);
    customer1.addOrder(order2);
    customer1.addOrder(order3);
    customer1.addOrder(order4);


    Customer customer2 = new Customer("Bulma", "Briefs", 2,
        "International", 5);

    Product product11 = new Product("Batteries", 70.99);
    Product product12 = new Product("Resistors", 90.99);
    Product product13 = new Product("Capacitors", 59.9);

    OrderItem orderItem11 = new OrderItem(1, 10000,product11);
    OrderItem orderItem12 = new OrderItem(2, 55555,product12);
    OrderItem orderItem13 = new OrderItem(3, 50000,product13);

    Order order5 = new InternationalOrder(1, 50);
    order5.addOrderItem(orderItem11);
    order5.addOrderItem(orderItem12);
    order5.addOrderItem(orderItem13);

    Product product14 = new Product("Circuit Boards", 99.99);
    Product product15 = new Product("Wire", 1.99);

    OrderItem orderItem14 = new OrderItem(1, 12000, product14);
    OrderItem orderItem15 = new OrderItem(2, 20000, product15);

    Order order6 = new InternationalOrder(2, 90);
    order6.addOrderItem(orderItem14);
    order6.addOrderItem(orderItem15);

    Product product16 = new Product("Solder", 99.99);

    OrderItem orderItem16 = new OrderItem(1, 12000, product16);

    Order order7 = new InternationalOrder(3, 10);
    order7.addOrderItem(orderItem16);

    customer2.addOrder(order5);
    customer2.addOrder(order6);
    customer2.addOrder(order7);

    printOrderPrice(customer1);
    printOrderPrice(customer2);
  }


  public static void printOrderPrice(Customer customer){
    double total = 0;

    System.out.println("Customer " + customer.getCustomerNumber() + ": " + customer.getName() +
        " " + customer.getSurname() +"(Age: " + customer.getCustomerAge() + ") " + "(" +
        customer.getCustomerType() + ")");

    for(Order order : customer.getOrders()){
      System.out.println("  Order " + order.getOrderNumber() + "(" + order.getOrderType() +
          ") ");

      for(OrderItem orderItem : order.getOrderItems()){
        System.out.println("    Order Item " + orderItem.getOrderItemNumber() + ": " + orderItem.getOrderItemName() +
            " R" +
            orderItem.getOrderItemPrice()* orderItem.getOrderItemQuantity());
      }

      System.out.println("  Total price of all items in order No." +order.getOrderNumber() + " is: R" +
          order.getOrderPrice()+ "\n");
      total += order.getOrderPrice();
    }

    System.out.println("Total price of all orders is: R" + total +
        "\n\n");
  }

}
