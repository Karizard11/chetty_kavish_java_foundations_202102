package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String name;
  private String surname;
  private String customerType;

  private int customerNumber;
  private int customerAge;
  List<Order> orders = new ArrayList();

  public Customer(final String name,final String surname,final int customerNumber,final String customerType,
                  final int customerAge) {
    this.name = name;
    this.surname = surname;
    this.customerNumber = customerNumber;
    this.customerType = customerType;
    this.customerAge = customerAge;
  }

  public void addOrder(Order order){
    if(order.getOrderType().equals("Local")& customerType.equals("Local")) {
      orders.add(order);
    }else{

      if(order.getOrderType().equals("International") & customerType.equals("International")) {
        orders.add(order);
      }else{
        System.out.println("International customers can only have international orders");
      }
    }


  }

  public List<Order> getOrders(){
    return orders;
  }

  public String getName(){
    return name;
  }
  public String getSurname(){
    return surname;
  }
  public int getCustomerNumber(){
    return customerNumber;
  }

  public String getCustomerType(){
    return customerType;
  }

  public int getCustomerAge(){
    return customerAge;
  }
}
