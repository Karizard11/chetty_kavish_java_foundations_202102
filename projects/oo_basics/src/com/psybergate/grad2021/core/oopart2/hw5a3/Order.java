package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  private int orderNumber;
  private int discountType;
  private Customer customer;
  private List<OrderItem> orderItems = new ArrayList();

  private static final int MIN_NO_OF_YEARS = 2;
  private static final int MAX_NO_OF_YEARS = 5;

  private static final int MIN_DISCOUNT_AMOUNT = 500000;
  private static final int MAX_DISCOUNT_AMOUNT = 1000000;


  public Order( int orderNumber, int discountType, Customer customer) {
    this.orderNumber = orderNumber;
    this.discountType = discountType;
    this.customer = customer;
  }

  public double calcDiscountRateBasedOnYears() {
    if (this.customer.getCustomerAge() > MIN_NO_OF_YEARS && this.customer.getCustomerAge() <= MAX_NO_OF_YEARS) {
      return 0.075;
    }
    if (this.customer.getCustomerAge() > MAX_NO_OF_YEARS) {
      return 0.125;
    }
    return 0;
  }

  public double calcDiscountRateBasedOnTotalPrice() {
    if (calcOrderTotal() > MIN_DISCOUNT_AMOUNT && calcOrderTotal() <= MAX_DISCOUNT_AMOUNT) {
      return 0.05;
    }
    if (calcOrderTotal() > MAX_DISCOUNT_AMOUNT) {
      return 0.1;
    }
    return 0;
  }

  public double getOrderPrice() {
    if (getDiscountType() == 1) {
      return applyDiscount(calcDiscountRateBasedOnYears());
    }
    if (getDiscountType() == 2) {
      return applyDiscount(calcDiscountRateBasedOnTotalPrice());
    }
    return 0;
  }

  private double calcTotalOrder() {
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += orderItem.getOrderItemPrice();
    }
    return orderTotal;
  }

  public double calcOrderTotal() {
    return calcTotalOrder();
  }

  public double applyDiscount(double discountRate) {
    return calcOrderTotal() * (1 - discountRate);
  }

  public boolean isLocal() {
    return false;
  }

  public boolean isInternational() {
    return false;
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public int getDiscountType() {
    return discountType;
  }

  public void addOrderItem(OrderItem orderitem) {
    orderItems.add(orderitem);
  }

  public abstract String getOrderType();

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

}
