package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account{

  private double overdraft;

  public CurrentAccount(){
    System.out.println("This is the Lower Class");
  }

  public CurrentAccount(int accountNumber, int balance, double overdraft){
    super(accountNumber, balance);
    this.overdraft = overdraft;

    System.out.println(accountNumber);
    System.out.println(balance);
    System.out.println(overdraft);
  }
}
