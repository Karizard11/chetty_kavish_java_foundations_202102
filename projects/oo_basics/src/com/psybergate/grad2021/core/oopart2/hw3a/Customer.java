package com.psybergate.grad2021.core.oopart2.hw3a;

import java.util.concurrent.Callable;

public class Customer {

  public static void main(String[] args) {
    Customer customer1 = new Customer("001", "John",
        21, "Address", "Local");
    Customer customer2 = new Customer("001", "Jake",
        21, "Address", "Local");

    System.out.println("customer1.equals(customer2) = " + customer1.equals(customer2));
  }

  public static final int MINIMUM_AGE = 18;

  private String customerNum;

  private String customerName;

  private int customerAge;

  private String customerAddress;

  private String customerType;

  public Customer(String customerNum, String customerName, int customerAge, String customerAddress,
                  String customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerType = customerType;
  }

  public static String getFullCustomerType() {
    return "Type: Customer";
  }

  public void increaseCustomerAge(int yearsToIncreaseBy) {
    customerAge += yearsToIncreaseBy;
  }

  public void decreaseCustomerAge(int yearsToDecreaseBy) {
    customerAge -= yearsToDecreaseBy;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "customerNum='" + customerNum + '\'' +
        ", customerName='" + customerName + '\'' +
        ", customerAge=" + customerAge +
        ", customerAddress='" + customerAddress + '\'' +
        ", customerType='" + customerType + '\'' +
        '}';
  }

  public boolean equals(Object object) {

    if (this == object) {
      return true;
    }

    if (object == null) {
      return false;
    }

    if (this.getClass() != object.getClass()) { // does account for child passing.
      return false;
    }

//    if (!(object instanceof Customer)) {//would still run if child of customer is passed in.
//      return false;
//    }

    Customer customer = (Customer) object;

    if (this.customerNum.equals(customer.customerNum)) {
      return true;
      //( do not need if) return this.customerNum.equals(customer.customerNum;
    }

    return false;
  }
}
