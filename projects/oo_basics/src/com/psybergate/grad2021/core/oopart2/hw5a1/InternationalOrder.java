package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class InternationalOrder extends Order{

  private double importDuties;
  private double orderPrice;
  private String orderType;
  private List<OrderItem> orderItems = new ArrayList();

  public  InternationalOrder( int orderNumber, double importDuties){
    super(orderNumber);
    this.importDuties = importDuties;
    this.orderType = "International";
    this.orderPrice = getOrderPrice();
  }

  public String getOrderType(){
    return orderType;
  }

  public double getOrderPrice(){
    double orderPrice = 0;

    for(OrderItem orderItem: orderItems){
      orderPrice += orderItem.getOrderItemPrice()*orderItem.getOrderItemQuantity()+importDuties;
    }
    return orderPrice;
  }

  public double getImportDuties() {
    return importDuties;
  }

  public void addOrderItem(OrderItem orderitem){
    orderItems.add(orderitem);
  }

  public List<OrderItem> getOrderItems(){
    return orderItems;
  }
}
