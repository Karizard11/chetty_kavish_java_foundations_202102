package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class Account {

  private int accountNumber;
  private int balance;



  public Account(){
    System.out.println("This is the Super Class");
  }

  public Account(int accountNumber, int balance){
    this.accountNumber = accountNumber;
    this.balance = balance;

    System.out.println(accountNumber);
    System.out.println(balance);
  }

}
