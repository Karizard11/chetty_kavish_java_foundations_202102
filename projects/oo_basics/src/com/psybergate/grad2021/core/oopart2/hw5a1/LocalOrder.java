package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order{

  private double discountRate;
  private String orderType;
  private List<OrderItem> orderItems = new ArrayList();

  public LocalOrder(int orderNumber, double discountRate){
    super(orderNumber);
    this.discountRate = discountRate;
    this.orderType = "Local";
  }

  public double getDiscountRate(){
    return discountRate;
  }

  public String getOrderType(){
    return orderType;
  }
  public double getOrderPrice(){
    double orderPrice = 0;

    for(OrderItem orderItem: orderItems){
      orderPrice += orderItem.getOrderItemPrice()*(1-discountRate)*orderItem.getOrderItemQuantity();
    }
    return orderPrice;
  }

  public void addOrderItem(OrderItem orderitem){
    orderItems.add(orderitem);
  }

  public List<OrderItem> getOrderItems(){
    return orderItems;
  }
}
