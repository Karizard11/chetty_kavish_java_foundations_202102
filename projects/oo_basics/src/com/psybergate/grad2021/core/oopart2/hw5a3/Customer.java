package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String name;
  private String surname;
  private String customerType;
  private int customerNumber;
  private int customerAge;

  List<Order> orders = new ArrayList();

  public Customer( String name, String surname, int customerNumber,
                   String customerType, LocalDate customerInitializedDate) {
    this.name = name;
    this.surname = surname;
    this.customerNumber = customerNumber;
    this.customerType = customerType;
    this.customerAge = LocalDate.now().getYear()-customerInitializedDate.getYear();
  }

  public boolean isLocalCustomer(){
    if(customerType.equals("Local") || customerType.equals("local")){
      return true;
    }
    return false;
  }

  public boolean isInternationalCustomer(){
    if(customerType.equals("International") || customerType.equals("international")){
      return true;
    }
    return false;
  }

  public void addOrder(Order order) {
    if (isLocalCustomer()) {
      if (!(order.isLocal())) {
        throw new RuntimeException("Local customers can only have Local Orders");
      }else{
        orders.add(order);
      }
    } else {
      if (isInternationalCustomer()) {
        if (!(order.isInternational())) {
          throw new RuntimeException("International customers can only have International Orders");
        }else{
          orders.add(order);
        }
      }
    }
  }

  public List<Order> getOrders(){
    return orders;
  }
  public String getName(){
    return name;
  }
  public String getSurname(){
    return surname;
  }
  public int getCustomerNumber(){
    return customerNumber;
  }
  public String getCustomerType(){
    return customerType;
  }
  public int getCustomerAge(){  return customerAge; }
}
