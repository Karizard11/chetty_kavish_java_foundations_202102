package com.psybergate.grad2021.core.oopart2.ce2a;

public class SomeObject {

  private int integer;

  public static void main(String[] args) {
    someMethod();
  }

  public SomeObject(int integer){
    this.integer = integer;
  }

  public static void swap(SomeObject a, SomeObject b){
    SomeObject c = b;
    SomeObject d = a;

    a= c;
    b= d;

    System.out.println(a.integer);
    System.out.println(b.integer);
  }

  public static void someMethod (){
    SomeObject a = new SomeObject (1); // call this Object SomeObject1
    SomeObject b = new SomeObject (2); // call this Object SomeObject2
    swap(a, b);

    System.out.println(a.integer);
    System.out.println(b.integer);
    // after this method call, the following should happen:
    // a should point to SomeObject2
    // b should point to SomeObject1
  }
}
