package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class InternationalOrder extends Order {

  private double importDuties;
  private String orderType;
  private List<OrderItem> orderItems = new ArrayList();

  public  InternationalOrder( int orderNumber, double importDuties){
    super(orderNumber);
    this.importDuties = importDuties;
    this.orderType = "International";
  }

  public double calcDiscountRate(){
    if(totalOrderPrice()> 500000 && totalOrderPrice() <= 1000000){
      return 0.05;
    }
    if(totalOrderPrice() > 1000000){
      return 0.1;
    }
    return 0;
  }

  public String getOrderType(){
    return orderType;
  }

  public double totalOrderPrice(){
    double orderPrice = 0;

    for(OrderItem orderItem: orderItems){
      orderPrice += orderItem.getOrderItemPrice()*orderItem.getOrderItemQuantity();
    }
    return orderPrice;
  }

  public double getOrderPrice(){
    return totalOrderPrice()*(1-calcDiscountRate()) + importDuties;
  }

  public void addOrderItem(OrderItem orderitem){
    orderItems.add(orderitem);
  }

  public List<OrderItem> getOrderItems(){
    return orderItems;
  }
}
