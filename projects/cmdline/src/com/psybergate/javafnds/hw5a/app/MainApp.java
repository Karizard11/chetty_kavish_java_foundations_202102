package com.psybergate.javafnds.hw5a.app;

import com.psybergate.javafnds.hw5a.service.TaxService;
import com.psybergate.javafnds.hw5a.service.impl.TaxServiceImpl;

/**
 * 
 * @since 19 Jan 2020
 */
public class MainApp {

  public static void main(String[] args) {
    TaxService taxService = new TaxServiceImpl();
    System.out.println("Calculated Tax for John Smith is: " + taxService.calculateTax("123456"));
  }

}
