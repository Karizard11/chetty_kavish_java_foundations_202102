package com.psybergate.javafnds.hw5a.service;

/**
 * 
 * @since 19 Jan 2020
 */
public interface TaxService {

  int calculateTax(String taxReferenceNum);

}
