package com.psybergate.javafnds.hw4a.a;

import com.psybergate.javafnds.hw4a.b.*;
import com.psybergate.javafnds.hw4a.c.*;

public class classA{
	public static void main(String[] args){
		System.out.println("In Main");
		System.out.println(classB.runner("a"));
		System.out.println("Back in Main");
	}

	public static String theEnd(String str){
		System.out.println("Back in A");
		return str;
	}
}