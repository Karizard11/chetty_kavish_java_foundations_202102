package com.psybergate.javafnds.hw3a.dir2.hw1b;

import com.psybergate.javafnds.hw3a.dir1.hw1a.MyStringUtils;
import java.util.Scanner;

public class StringReverse{

	public static void main(String[] args){

		Scanner scan_set_up = new Scanner(System.in);
		String inputName;

		System.out.println("Enter your name: ");

		inputName = scan_set_up.next();
		
		reverse(inputName);
	}

	public static void reverse(String str){
		System.out.println("Enjoy the reverse: \r\n" + MyStringUtils.reverse(str));
		System.out.println("Try pronouncing that! I dare you!");
	}
	
}