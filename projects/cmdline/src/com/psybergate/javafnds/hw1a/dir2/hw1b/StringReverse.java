package com.psybergate.javafnds.hw1a.dir2.hw1b;

import com.psybergate.javafnds.hw1a.dir1.hw1a.*;
import java.util.Scanner;

public class StringReverse{

	public static void main(String[] args){

		Scanner scanSetUp = new Scanner(System.in);
		String inputName;

		System.out.println("Enter your name: ");

		inputName = scanSetUp.next();

		reverse(inputName);
	}

	public static void reverse(String str){
		System.out.println("Enjoy the reverse: \r\n" + MyStringUtils.reverse(str));
		System.out.println("Try pronouncing that! I dare you!");
	}
	
}