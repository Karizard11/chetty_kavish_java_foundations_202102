package com.psybergate.javafnds.hw2a.dir1.hw1a;

public class MyStringUtils {

	public static String reverse(String str){

		char[] letterArr = str.toCharArray();
		char[] revArr = new char[letterArr.length];

		for(int i = 0; i < letterArr.length; i++){

			revArr[i] = letterArr[letterArr.length-1-i];
		}

		return String.valueOf(revArr);
	}
}