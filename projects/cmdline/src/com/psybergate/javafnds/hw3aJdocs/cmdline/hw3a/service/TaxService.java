package com.psybergate.javafnds.hw3aJdocs.cmdline.hw3a.service;

/**
 * 
 * @since 19 Jan 2020
 */

public interface TaxService {

  int calculateTax(String taxReferenceNum);

}
