package com.psybergate.javafnds.hw3aJdocs.cmdline.hw3a.domain;

public class Person {

  private String taxReferenceNum;

  private String name;

  private String surname;

  private int salary;

  public Person(String taxReferenceNum, String name, String surname, int salary) {
    this.taxReferenceNum = taxReferenceNum;
    this.name = name;
    this.surname = surname;
    this.salary = salary;
  }

  public int getSalary() {
    return salary;
  }
}
