package com.psybergate.javafnds.hw3a;

// import java.util.stream.IntStream;

public class UnicodeStream {
  public static void main( String[] args ) {

    for( int i = 0; i <= 8000; i++ )
      System.out.println( "Char: " + i + "/(hex) " + Integer.toHexString(i)+ " ==> " + (char)i );
  }

  /* public static void main(String[] args) {
  //   IntStream.range(0, 2000).mapToObj(i -> "Char: " + i + " ==> " + new String(Character.toChars(i))).forEach(System.out::println);
   }
  */

}
