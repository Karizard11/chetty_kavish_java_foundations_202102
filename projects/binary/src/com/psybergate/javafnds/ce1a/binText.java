package com.psybergate.javafnds.ce1a;

import java.io.*;

public class binText {
    public static void main(String[] args)
    {
        File file = new File("binary\\src\\com\\psybergate\\javafnds\\ce1a\\Text01.txt"); //initialize File object and passing path as argument
        boolean result;
        try{
            result = file.createNewFile();  //creates a new file
            // test if successfully created a new file
            if(result){
                System.out.println("file created "+file.getCanonicalPath()); //returns the path string
            }
            else
            {
                System.out.println("File already exist at location: "+file.getCanonicalPath());
            }
            FileWriter myWriter = new FileWriter("binary\\src\\com\\psybergate\\javafnds\\ce1a\\Text01.txt");
            myWriter.write("These are not the droids you are looking for.");
            myWriter.close();

            try (FileReader fr = new FileReader(file)) {
                char[] chars = new char[(int) file.length()];

                fr.read(chars);

                String fileContent = new String(chars);

                System.out.println("This is in the file: " + fileContent);
                System.out.println("This is the Decimal equivalent: " + convertStringToDec(fileContent));
                System.out.println("This is the Hex equivalent: " + convertStringToHex(fileContent));
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();    //prints exception if any
        }
    }

    // Char -> Decimal -> Hex
    public static String convertStringToHex(String str) {

        StringBuilder hex = new StringBuilder();
        // loop chars one by one
        int cnt = 0;

        for (char temp : str.toCharArray()) {
            // convert int to hex 61
            hex.append(Integer.toHexString(temp));
            hex.append("|");
        }
        hex.deleteCharAt(hex.length() -1);

        return hex.toString();
    }
    // Char -> Decimal
    public static String convertStringToDec(String str) {

        StringBuilder dec = new StringBuilder();
        // loop chars one by one
        for (char temp : str.toCharArray()) {
            // convert char to int, for char `a` decimal 9
            dec.append((int) temp);
            dec.append("|");
        }
        dec.deleteCharAt(dec.length() -1);

        return dec.toString();
    }
}