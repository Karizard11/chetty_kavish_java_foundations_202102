package com.psybergate.javafnds.ce3a;

public class minMax {
  public static void main(String[] args) {

    System.out.println("The minimum value of a byte is: " + Byte.MIN_VALUE);
    System.out.println("The maximum value of a byte is: " + Byte.MAX_VALUE);

    System.out.println("The minimum value of a short is: " + Short.MIN_VALUE);
    System.out.println("The maximum value of a short is: " + Short.MAX_VALUE);

    System.out.println("The minimum value of a integer is: " + Integer.MIN_VALUE);
    System.out.println("The maximum value of a integer is: " + Integer.MAX_VALUE);

    System.out.println("The minimum value of a long is: " + Long.MIN_VALUE);
    System.out.println("The maximum value of a long is: " + Long.MAX_VALUE);

    System.out.println("The minimum value of a float is: " + Float.MIN_VALUE);
    System.out.println("The maximum value of a float is: " + Float.MAX_VALUE);

    System.out.println("The minimum value of a double is: " + Double.MIN_VALUE);
    System.out.println("The maximum value of a double is: " + Double.MAX_VALUE);

  }
}
