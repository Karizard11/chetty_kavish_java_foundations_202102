package com.psybergate.javafnds.hw1a;

import java.io.*;

public class BytesIn {
  public static void main(String[] args) {
    File file = new File("C:\\myworkbench\\mymentoring\\java_foundations_202102\\projects\\bin\\production\\binary\\com\\psybergate\\javafnds\\hw1a\\SimpleText.class");
    StringBuilder builder = new StringBuilder();
    try {
      FileInputStream fin = new FileInputStream(file);
      byte[] buffer = new byte[1024];
      int bytesRead = 0;
      while((bytesRead = fin.read(buffer)) > -1)
        for(int i = 0; i < bytesRead; i++)
          builder.append(String.format("%02x", buffer[i] & 0xFF)).append(i != bytesRead - 1 ? " " : "");
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(builder.toString());
  }
}
